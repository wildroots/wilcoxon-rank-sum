\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Definition Wilcoxon Rank-Sum Test},
            pdfauthor={Notes by Crista Moreno},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage[margin=1in]{geometry}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\usepackage{mathrsfs}
\newtheorem{mydef}{Definition}
\usepackage{thmtools}
\declaretheoremstyle[headfont=\normalfont]{normalhead}

\title{Definition Wilcoxon Rank-Sum Test}
\author{Notes by Crista Moreno}
\date{}

\begin{document}
\maketitle

Notes from \emph{Introduction to the Theory of Statistics} by Mood,
Gray, et al.

A nonparametric test described by Frank Wilcoxon (1892-1965) and studied
by Mann and Whitney.

Given two random samples \(X_{1}, X_{2}, \dots, X_{m}\) and
\(Y_{1}, Y_{2}, \dots, Y_{n}\) from populations with absolutely
continuous c.d.f.'s \(F_{X}(\cdot)\) and \(F_{Y}(\cdot)\), respectively,
one arranges the \(m + n\) observations in ascending order and then
replaces the smallest observations by 1, the next by 2, and so on, the
largest being replaced by \(m + n\). These integers are called the
\textit{ranks} of the observations.

Let \(T_{x}\) denote the sum of the ranks of the \(m\) \(x\) values and
\(T_{y}\) denote the sum of the ranks of the \(n\) \(y\) values.

\[T_{x} = 1 + 2 + 3 + \cdots + (m - 2) + (m - 1) + m\]
\[ = 1 + (m - 1) + 2 + (m - 2) + 3 + (m - 3) + \cdots + m = \dfrac{m(m + 1)}{2}\]
Note that

\[T_{x} + T_{y} = \sum_{j = 1}^{m + n}j = \dfrac{(m + n + 1)(m + n)}{2}\]

So \(T_{y}\) is a linear function of \(T_{x}\). Let us base a test on
the statistic \(T_{x}\).

\(T_{x}\) is linearly related to another statistic, which is denoted by
\(U\). Set

\[U = \sum_{j = 1}^{n}\sum_{i = 1}^{m} I_{[Y_{j}, \infty]}(X_{i})\]
where \(U\) is the number of times an \(X\) exceeds a \(Y\).

Let \(r_{1}, r_{2}, \dots, r_{m}\) denote the ranks of the \(x\) values,
and let \(x_{1}', \dots, x_{m}'\) denote the ordered \(x\) values.
\(x_{1}'\) exceeds \((r_{1} - 1)\) \(y\) values, \(x_{2}'\) exceeds
\((r_{2} - 2)\) \(y\) values, and so on, and \(x_{m}'\) exceeds
\((r_{m} - m)\) \(y\) values. Hence,

\[u = \sum_{i = 1}^{m} (r_{i} - i) = \sum r_{i} - \sum i = t_{x} - \dfrac{m(m + 1)}{2}\]

\[U = T_{x} - \dfrac{m(m + 1)}{2}\]

To find the first two moments of \(T_{x}\), we find the first two
moments of \(U\).

\[\mathscr{E}[U] = \mathscr{E}\left[\sum\sum I_{[Y_{j, \infty}]} (X_{i})\right] = \sum\sum \mathscr{E}[I_{[Y_{j}, \infty]} (X_{i})]\]

\[= \sum \sum P[X_{i} \geq Y_{j}] = \sum \sum p = mnp\]

where
\(p = P[X_{i} \geq Y_{j}] = \int P[Y \leq x | X = x] f_{X}(x) dx = \int F_{Y}(x)f_{X}(x)dx\).

If \(\mathscr{H}_{0}\) is true,

\[p = \int F_{X}(x)f_{X}(x) dx = \int_{0}^{1} u du = \dfrac{1}{2}\]

\end{document}
